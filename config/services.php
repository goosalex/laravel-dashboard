<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'laravelpassport' => [
        'client_id' => env('OAUTH2_KEY'),
        'client_secret' => env('OAUTH2_SECRET'),
        'redirect' => env('OAUTH2_REDIRECT_URI'),
        'host' => env('OAUTH2_HOST'),
        'authorize_uri' => env('OAUTH2_AUTHORIZE_URI'),
        'token_uri' => env('OAUTH2_TOKEN_URI'),
        'userinfo_uri' => env('OAUTH2_USERINFO_URI'),
        'userinfo_key' => env('OAUTH2_USERINFO_KEY'),
        'user_id'   => env('OAUTH2_USER_ID'),
        'user_name'   => env('OAUTH2_USER_NAME'),
        'user_nickname'   => env('OAUTH2_USER_NICKNAME'),
        'user_email'   => env('OAUTH2_USER_EMAIL'),
        'user_avatar'   => env('OAUTH2_USER_AVATAR')
    ],

];

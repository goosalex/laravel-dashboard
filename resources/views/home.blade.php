@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>

    <div id="navigation-content" class="flex width-100">
        <div id="logo"><img src="/images/dachs-white.png"></div>
        <div class="inner flex width-100 justify-content-spacearound">
            <ul class="grid-col-50">
                <li><a href="{{ url('/todo') }}">ToDo</a></li>
                <li><a href="/?logout=true">Logout</a></li>
            </ul>
        </div>
    </div>

</div>
@endsection

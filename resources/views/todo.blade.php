@extends('layouts.app')
@section('content')
<script type="application/javascript" src="{{ URL::asset('js/todo.js') }}"></script>
<div id='wrapper' class="flex justify-content-spacearound todo">
    <div id="content" class="grid-col-60">

        <?php

        ///////////////////
        //fetch customers
        ///////////////////

        $data = file_get_contents("../json/missionOrder-list-today.json");
        $json_a = json_decode($data,true);


        $data_array =[];

        $newDate = new DateTime();
        $now = $newDate->getTimestamp();
        $today = date("d.m.Y",time());


        echo $filter_mate;
        ?>

        <?php if(empty($filter_mate)){ ?>
        <form method="get" action="{{ url('/todo') }}">
            <div id="top-bar">
                <div class="flex grid-col-60 justify-content-spacebetween">
                    <select name="mate" id="mate" class="grid-col-60" placeholder="mate" onchange="this.form.submit()">
                    <?php

                        $mates = [];
                        foreach($json_a as $key=>$value) {
                            $mates[$json_a[$key]["realisedBy"]] = $json_a[$key]["realisedBy"];
                        }
                        foreach ($mates as $mate) {
                            ?>
                            <option value="{{ $mate }}">{{$mate}}</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </form>
        <?php } else { ?>
        <a href="{{ url('/todo') }}">Alle</a>
        <?php
        }
        foreach($json_a as $key=>$value)
        {


            $product = [];
            $products = "";
            //unix timestamp
            $dateF = "";
            $date ="";


            if($json_a[$key]["meta.summaries.startDate"] != "")
            {
                $dateF = date("d.m.Y",strtotime($json_a[$key]["meta.summaries.startDate"]));
            }


            //push & merge products
            for($i = 0; $i < 15; $i++)
            {
                if($json_a[$key]["positions[".$i."].meta.summaries.info"] != "")
                {
                    array_push($product,
                        $json_a[$key]["positions[".$i."].meta.summaries.info"]
                    );

                }
            }



            if($filter_mate != ""):
                if($json_a[$key]["realisedBy"] === $filter_mate && $dateF == $today):
                    $products = join("<br>",$product);

                    array_push($data_array,[
                        "timestamp"=> strtotime($dateF),
                        "date"=> $dateF,
                        "time"=>$json_a[$key]["meta.summaries.startTime"],
                        "pick"=>$json_a[$key]["meta.summaries.startShortInfo"],
                        "drop"=>$json_a[$key]["meta.summaries.endShortInfo"],
                        "billingCustomer"=>$json_a[$key]["billingCustomerId"],
                        "mate"=>$json_a[$key]["realisedBy"],
                        "stops"=>$json_a[$key]["meta.summaries.stepsInfo"],
                        "price"=>$json_a[$key]["sums.base"],
                        "products"=>$products,
                        "meta.status"=>$json_a[$key]["meta.status"],
                        "status"=>$json_a[$key]["status"]

                    ]);
                endif;
            endif;

            if(empty($filter_mate)):
                if($dateF == $today):
                    $products = join("<br>",$product);
                    array_push($data_array,[
                        "timestamp"=> strtotime($dateF),
                        "date"=> $dateF,
                        "time"=>$json_a[$key]["meta.summaries.startTime"],
                        "pick"=>$json_a[$key]["meta.summaries.startShortInfo"],
                        "drop"=>$json_a[$key]["meta.summaries.endShortInfo"],
                        "billingCustomer"=>$json_a[$key]["billingCustomerId"],
                        "mate"=>$json_a[$key]["realisedBy"],
                        "stops"=>$json_a[$key]["meta.summaries.stepsInfo"],
                        "price"=>$json_a[$key]["sums.base"],
                        "products"=>$products,
                        "meta.status"=>$json_a[$key]["meta.status"],
                        "status"=>$json_a[$key]["status"]

                    ]);
                endif;
            endif;


        }



        //sort array by date
        uasort($data_array, function($a, $b) {
            return strcmp($a['time'], $b['time']);
        });

        /*
        echo '<pre>';
        print_r($data_array);
        echo '<pre>';
        */





        foreach($data_array as $key=>$value)
        {
            if($data_array[$key]['status'] == "ordered" || $data_array[$key]['status'] == "produced"):
                if($data_array[$key]['meta.status'] == "published"):

                    echo "<article class='grid-col-60'>";
                    echo "<div class='timestamp'>".$data_array[$key]['timestamp']."</div>";
                    echo "<div class='date'>".$data_array[$key]['date']."</div>";
                    echo "<div class='time'>".$data_array[$key]['time']."</div>";
                    echo "<div class='pick'>".$data_array[$key]['pick']."</div>";
                    echo "<div class='drop'>".$data_array[$key]['drop']."</div>";
                    echo "<div class='stops'>".$data_array[$key]['stops']."</div>";
                    echo "<div class='price'>".$data_array[$key]['price']."</div>";
                    echo "<div class='status'>".$data_array[$key]['status']."</div>";
                    echo "<div class='status'>".$data_array[$key]['meta.status']."</div>";
                    echo "<div class='mate'>".$data_array[$key]['mate']."</div>";

                    echo "</article>";

                endif;
            endif;

        }


        ?>


        <?php // include("pages/footer.php") ?>



    </div>

</div>
@endsection

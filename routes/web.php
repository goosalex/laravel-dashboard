<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/todo', function () {
    $mate = request('mate');
    return view('todo', [ 'filter_mate' => $mate ]);
})->middleware('auth');

Route::get('/pwd', function () {
    return view('pwd');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/externallogin', 'Auth\ExternalLoginController@redirectToProvider')->name("externallogin");
Route::get('/redirect',      'Auth\ExternalLoginController@handleProviderCallback');
